import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// Open browser and navigate to the website
WebUI.openBrowser('')

WebUI.maximizeWindow(FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl('https://perqara.com/')

WebUI.delay(1)

// Click on the "Masuk" button on the landing page
WebUI.click(findTestObject('Website/Landing_Page/button_Masuk_Landing_Page'))

WebUI.delay(1)

// Enter valid email and password
WebUI.setText(findTestObject('Website/Landing_Page/Modal_Login/input_Email'), GlobalVariable.email)

WebUI.setText(findTestObject('Website/Landing_Page/Modal_Login/input_Password'), GlobalVariable.password)

WebUI.delay(1)

// Click the "Masuk" button in the login modal
WebUI.click(findTestObject('Website/Landing_Page/Modal_Login/button_Masuk_Modal_Login'))

WebUI.delay(15)

// Enter the extracted OTP code in the login modal
WebUI.setText(findTestObject('Object Repository/Website/Landing_Page/Modal_Login/input_OTP'), GlobalVariable.myGlobalOTP)

println(('****' + GlobalVariable.myGlobalOTP) + '****')

// Click the "Masuk" button in the OTP modal
WebUI.click(findTestObject('Website/Landing_Page/Modal_OTP/button_Masuk_Modal_OTP'))

WebUI.delay(5)

//Verify successfully logged in to the Perqara website
WebUI.verifyElementPresent(findTestObject('Website/Home_Page/div_profile'), 0)

println(('****' + GlobalVariable.myGlobalOTP) + '****')