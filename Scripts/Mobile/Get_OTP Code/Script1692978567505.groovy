import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication('com.google.android.gm')

// Get the text from the object
def text = Mobile.getText(findTestObject('Object Repository/Mobile/Email_Content'), 0)

// Apply a regex pattern to extract the OTP
def regexPattern = '\\b\\d{6}\\b'

def matcher = text =~ regexPattern

// Check if the OTP is found
if (matcher.find()) {
    // Extract the OTP from the regex match
    def extractedOTP = matcher.group(0)

    // Set the extracted OTP as a global variable
    GlobalVariable.myGlobalOTP = extractedOTP // Handle case when OTP is not found
} else {
    GlobalVariable.myGlobalOTP = 'OTP not found'
}

Mobile.delay(30, FailureHandling.STOP_ON_FAILURE)

println(('****' + GlobalVariable.myGlobalOTP) + '****')