<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_profile</name>
   <tag></tag>
   <elementGuidId>d67953da-f4a1-478d-afd3-d5ccb9fab254</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.bg-white.rounded-lg.w-40.h-9.shadow-lg > div.flex.items-center.h-9.pl-1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//button[@id='wrap-profile']/div/div)[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9b4a5865-6981-46b7-9449-25b1219496b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>flex items-center h-9 pl-1</value>
      <webElementGuid>2e1224c2-89f2-4720-92be-52a9930a675a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>samuel pangestu</value>
      <webElementGuid>604d0a3d-6073-41f5-a486-d41ac162247d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-default&quot;)/button[@id=&quot;wrap-profile&quot;]/div[@class=&quot;bg-white rounded-lg w-40 h-9 shadow-lg&quot;]/div[@class=&quot;flex items-center h-9 pl-1&quot;]</value>
      <webElementGuid>bf8230ef-f541-4e6d-b353-b0ef6df64c6e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//button[@id='wrap-profile']/div/div)[3]</value>
      <webElementGuid>bcf689fe-3941-4dfa-b176-fda72fd0186c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar'])[1]/following::div[2]</value>
      <webElementGuid>d6bd8f11-6898-4dee-bc58-3d427db64b6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[1]/following::div[2]</value>
      <webElementGuid>35d6640d-46c7-4f1b-8ea9-2cbd2c0de311</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Konsultasi Sekarang'])[1]/preceding::div[10]</value>
      <webElementGuid>f3f095df-6a7f-430b-a41b-acb8f565bf17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ada Masalah Hukum? Perqara Siap Bantu Cari Solusinya!'])[1]/preceding::div[19]</value>
      <webElementGuid>c5ed2b0a-e17f-4021-b348-306ba1443203</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/button/div/div</value>
      <webElementGuid>326e81ab-9c8d-4444-b826-cb4857499c46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'samuel pangestu' or . = 'samuel pangestu')]</value>
      <webElementGuid>f2a5a626-43d4-4e9c-a990-feab96a87c90</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
