<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Masuk_Modal_OTP</name>
   <tag></tag>
   <elementGuidId>4c9c408a-68fb-4552-bbf3-5c21df90023c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.py-2.w-full.text-white.rounded-md.bg-blue-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='loginVerification']/div[7]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>b70f1b25-6546-42ad-88fa-edd72cd02fac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>py-2 w-full text-white rounded-md bg-blue-2</value>
      <webElementGuid>6d6f8a11-d474-441a-b393-883cefec0761</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Masuk </value>
      <webElementGuid>356699f4-15e7-4b03-bae4-9c8c19d476ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loginVerification&quot;)/div[@class=&quot;py-12 px-10&quot;]/button[@class=&quot;py-2 w-full text-white rounded-md bg-blue-2&quot;]</value>
      <webElementGuid>687b4ed9-5ada-4818-8927-7310a5260f00</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='loginVerification']/div[7]/button</value>
      <webElementGuid>0a57ef33-2883-4de6-88e7-23cc87e81a31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ubah disini'])[1]/following::button[1]</value>
      <webElementGuid>1ad4b511-34c0-4690-a2e2-ca3b07c62844</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kirim Kode Baru'])[1]/following::button[1]</value>
      <webElementGuid>a0921633-823e-4e6e-955e-bf4b74042147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[2]/preceding::button[3]</value>
      <webElementGuid>37c8ab97-ee7e-41bc-8130-8e8788c20323</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar'])[1]/preceding::button[4]</value>
      <webElementGuid>7cdf8f78-ed84-4b0e-9fa8-5c1869a2fed0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Masuk']/parent::*</value>
      <webElementGuid>56e31ad6-17f7-46c3-b9d5-73d32a73825d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/button</value>
      <webElementGuid>4971b149-2d93-4f34-8c74-834ca3f545f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = ' Masuk ' or . = ' Masuk ')]</value>
      <webElementGuid>ba01abd8-e47a-4687-a40c-8117fb161f9a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
