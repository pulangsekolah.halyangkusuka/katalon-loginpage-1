<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_OTP</name>
   <tag></tag>
   <elementGuidId>0a30d6e3-3083-48f2-a73b-a17de320a3ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>32c73030-25a6-4fd7-a36e-8c0fbd443509</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>ed13bbe7-4bc7-48bc-999b-d9ac0e9cf664</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>font-dm-sans</value>
      <webElementGuid>27ca65e3-8fc3-4895-a552-649bc74cb116</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>94a37d91-41c5-4350-994c-8852dbbddb12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>9b0f0e9c-a0af-4875-8c47-1bae8843c0ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>2859743b-e372-48ed-9579-b2e59ad08063</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;0&quot;)</value>
      <webElementGuid>b1a04fb8-2368-4ccc-aed3-fecf63b150b1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='0']</value>
      <webElementGuid>4e633945-768e-46f3-a25f-e67e12f817f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='input_0']/input</value>
      <webElementGuid>ed3f3b05-3535-456e-a248-d7b3ac543340</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>24f0047f-2cf6-49ec-b1a3-50224240f14f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = '0' and @type = 'text' and @placeholder = '0']</value>
      <webElementGuid>c99ae48a-171d-4c99-8dd0-b5846d5d4f11</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
