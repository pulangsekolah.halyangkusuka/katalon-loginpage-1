@Login
Feature: Login to Perqara Website

  @validlogin
  Scenario: Logging in with valid credentials and OTP
    Given User opens the browser and navigates to the Perqara website
    When User clicks on the Masuk button on the landing page
    And User enters their valid email and password
    And User clicks the Masuk button in the login modal
    And User enters the OTP code in the OTP modal
    And User clicks the Masuk button in the OTP modal
    Then User should be successfully logged in to the Perqara website
