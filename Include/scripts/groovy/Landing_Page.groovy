import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Landing_Page {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User opens the browser and navigates to the Perqara website")
	def open_and_navigate_to_website() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow(FailureHandling.STOP_ON_FAILURE)
		WebUI.navigateToUrl('https://perqara.com/')
		WebUI.delay(1)
	}

	@When("User clicks on the Masuk button on the landing page")
	def click_on_button_Masuk_Landing_Page() {
		WebUI.click(findTestObject('Website/Landing_Page/button_Masuk_Landing_Page'))
		WebUI.delay(1)
	}

	@And("User enters their valid email and password")
	def input_valid_email_password() {
		WebUI.setText(findTestObject('Website/Landing_Page/Modal_Login/input_Email'), GlobalVariable.email)		
		WebUI.setText(findTestObject('Website/Landing_Page/Modal_Login/input_Password'), GlobalVariable.password)		
		WebUI.delay(1)
	}
	
	@And("User clicks the Masuk button in the login modal")
	def click_on_button_Masuk_Modal_Login() {
		WebUI.click(findTestObject('Website/Landing_Page/Modal_Login/button_Masuk_Modal_Login'))
		WebUI.delay(1)
	}
	
	@And("User enters the OTP code in the OTP modal")
	def click_on_button_Masuk_Modal_Login(String OTPExtractedCode) {
		WebUI.setText(findTestObject('Object Repository/Website/Landing_Page/Modal_Login/input_OTP'), GlobalVariable.OTPExtractedCode)
		WebUI.delay(5)
	}
	
	@And("User clicks the Masuk button in the OTP modal")
	def click_on_button_Masuk_Modal_OTP() {
		WebUI.click(findTestObject('Website/Landing_Page/Modal_OTP/button_Masuk_Modal_OTP'))
	}
	
	@Then("User should be successfully logged in to the Perqara website")
	def verify_successfully_login() {
		WebUI.verifyElementPresent(findTestObject('Website/Home_Page/div_profile'), 0)
	}
	
}